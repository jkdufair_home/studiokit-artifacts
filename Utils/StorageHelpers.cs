﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using StudioKit.Artifacts.Properties;

namespace StudioKit.Artifacts.Utils
{
	public class StorageHelpers
	{
		private const string TextStorageLocation = "textartifacts/";
		private const string FileStorageLocation = "fileartifacts/";
		private const string GeneralTextArtifactFileName = "TextArtifact";
		private const string GeneralTextArtifactFileExtension = ".txt";

		public static string GenerateTextBlobPath()
		{
			return $"{TextStorageLocation}{GeneralTextArtifactFileName}-{Guid.NewGuid()}{GeneralTextArtifactFileExtension}";
		}

		public static string GenerateFileBlobPath(string fileName)
		{
			var fileExtension = Path.GetExtension(fileName);
			return $"{FileStorageLocation}{Guid.NewGuid()}{fileExtension}";
		}

		public static string GetExistingTextBlobPath(string textGuid)
		{
			return $"{TextStorageLocation}{textGuid}";
		}

		public static string GetExistingFileBlobPath(string fileGuid)
		{
			return $"{TextStorageLocation}{fileGuid}";
		}

		public static string GetFileBlobPathFromUrl(string url)
		{
			var match = Regex.Match(url, $"({FileStorageLocation}.+$)");
			if (match.Groups.Count != 2 || string.IsNullOrEmpty(match.Groups[1].Value))
				throw new ApplicationException(Strings.ArtifactUrlInvalid);
			return match.Groups[1].Value;
		}

		public static string GetTextBlobPathFromUrl(string url)
		{
			var match = Regex.Match(url, $"({TextStorageLocation}.+$)");
			if (match.Groups.Count != 2 || string.IsNullOrEmpty(match.Groups[1].Value))
				throw new ApplicationException(Strings.ArtifactUrlInvalid);
			return match.Groups[1].Value;
		}

		public static Stream GenerateStreamFromString(string content)
		{
			var stream = new MemoryStream();
			var writer = new StreamWriter(stream);
			writer.Write(content);
			writer.Flush();
			stream.Position = 0;
			return stream;
		}
	}
}