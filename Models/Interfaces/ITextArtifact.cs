﻿namespace StudioKit.Artifacts.Models.Interfaces
{
	public interface ITextArtifact : IArtifact
	{
		int WordCount { get; set; }
	}
}