﻿using System;
using System.Collections.Generic;

namespace StudioKit.Artifacts.Models.Interfaces
{
	public interface IArtifactsContainer<TArtifact>
		where TArtifact : IArtifact
	{
		ICollection<TArtifact> Artifacts { get; set; }
	}
}