﻿namespace StudioKit.Artifacts.Models.Interfaces
{
	public interface IArtifact
	{
		string Url { get; set; }

		bool IsDeleted { get; set; }

		string CreatedById { get; set; }
	}
}